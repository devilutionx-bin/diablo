﻿
![picture](https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/3a049cbe-4547-4bfd-941f-e0ad9f483c57/d4gper1-8b11bb34-71b1-4c8d-8352-9229719a6144.png/v1/fill/w_254,h_254/diablo_1_icon_by_mulek169_d4gper1-fullview.png) ![picture](https://cdn2.steamgriddb.com/file/sgdb-cdn/logo/2acb3374193eacfdc8c704b0faab6fee.png) 

Diablo 1 setup/launch script powered by DevilutionX, this is also a set of AUR packages.

Credit to the icon used in this project goes to Mulek169
[Link to icon here:](https://www.deviantart.com/mulek169/art/Diablo-1-Icon-269924077)

Packages: 
[devilutionx-bin](https://aur.archlinux.org/packages/devilutionx-bin))
[diablo](https://aur.archlinux.org/packages/diablo))

 ### Author
  * Corey Bruce
